var jsontTemplate = { 

    "self": "{@schema($)}", 

    "schema": function(obj) {
        
        var output = {};
        
        output.name = input.identifier;
        output.title = input.title;
        output.notes = input.description;

        output.identifier = [input.identifier];
        
        if (input.publisher) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
            output.publisher.name = input.publisher.name;
            if (input.publisher.publisherType) {
                output.publisher.publisher_type = input.publisher.publisherType;
            }            
            if (input.publisher.mbox) {
                output.publisher.email = input.publisher.mbox;
            }            
        }

        if (input.contactPoint && (input.contactPoint.fn || input.contactPoint.hasEmail)) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            if (input.contactPoint.hasEmail) {
            	output.contact_point[0].name = "mailto:" + input.contactPoint.hasEmail;
            }
            if (input.contactPoint.hasEmail) {
                output.contact_point[0].email = input.contactPoint.hasEmail;
            }            
        }

        if (input.keyword) {
            output.tags = [];
            for (var tag in input.keyword) {
                output.tags.push({"name": input.keyword[tag]});
            }
        }

        if (input.landingPage) output.url = input.landingPage;
        if (input.language) output.language = [{"resource": input.language}];
        if (input.modified) output.modified = input.modified;
        if (input.issued) output.modified = input.issued;

        if (input.distribution) {
            output.resources = [];
            for (var res in input.distribution) {
                var resource = {};
                resource.access_url = [input.distribution[res].accessURL];
                if (input.distribution[res].downloadURL) resource.download_url = [input.distribution[res].downloadURL];
                if (input.distribution[res].title) resource.name = input.distribution[res].title;
                if (input.distribution[res].description) resource.description = input.distribution[res].description;
                if (input.distribution[res].format) resource.format = input.distribution[res].format;
                if (input.distribution[res].mediaType) resource.mimetype = input.distribution[res].mediaType;
                output.resources.push(resource);            
            }
        }

        return JSON.stringify(output);
    }
};
