var jsontTemplate = { 

    "self": "{@schema($)}", 

    "schema": function(obj) {
        
        var output = {};
        
        output.name = input.name;
        output.title = input.title;
        output.notes = input.notes;

        if (input.organization) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Organization";
            output.publisher.name = input.organization.title;
        }

		if (!output.publisher && input.author) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
			output.publisher.name = input.author;
			if (input.author_email) output.publisher.email = "mailto:" + input.author_email.trim()
		}

        if (input["contact-name"]) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input["contact-name"];
        }

		if (input["contact-email"]) {
			if (!output.contact_point) {
				output.contact_point = [{}];
	            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            }
            output.contact_point[0].email = "mailto:" + input["contact-email"].trim();						
		}
		
        if (input.license_id) output.license_id = input.license_id;
        if (input.url) output.url = input.url;

        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                var t = {};
                t.name = input.tags[tag].name;
                output.tags.push(t);
            }
        }

        if (input["theme-primary"]) {
            var groups = {};

            //Agriculture, Fisheries, Forestry, foods
            groups["Agriculture"] = "agriculture-fisheries-forestry-and-food";
            //Education, culture and sport
            groups["Education"] = "education-culture-and-sport";
            //Environment
            groups["Environment"] = "environment";
            //Energy
            //Transport
            groups["Transport"] = "transport";
            //Science and technology
            //Economy and finance
            groups["Business & Economy"] = "economy-and-finance";
            //Population and social conditions
            groups["Society"] = "population-and-society";
            //Health
            groups["Health"] = "health";
            //Government, Public sector
            groups["Government"] = "government-and-public-sector";
            groups["Government Spending"] = "government-and-public-sector";
            //Regions; Cities
            groups["Towns & Cities"] = "regions-and-cities";
            groups["Mapping"] = "regions-and-cities";
            //Justice,  legal system, public safety
            groups["crime and justice"] = "justice-legal-system-and-public-safety";
            //International issues
            groups["Defence"] = "international-issues";

            output.groups = [];
            output.groups.push({"name" : groups[input["theme-primary"]]});

            if (input["theme-secondary"]) {
                for (var group in input["theme-secondary"]) {
                    output.groups.push({"name" : groups[input["theme-secondary"][group]]});
                }
            }
        }

		if (input["temporal_coverage-from"]) {
			output.temporal = [{}];
			try {
				output.temporal[0].start_date = JSON.parse(input["temporal_coverage-from"])[0];
			} catch (e) {
				output.temporal[0].start_date = input["temporal_coverage-from"];			
			} 
		}
		if (input["temporal_coverage-to"]) {
			if (!output.temporal) output.temporal = [{}];
			try {
				output.temporal[0].end_date = JSON.parse(input["temporal_coverage-to"])[0];
			} catch (e) {
				output.temporal[0].end_date = input["temporal_coverage-to"];			
			} 
		}
        
        if (input.extras) {
            for (var extra in input.extras) {
                var key = input.extras[extra].key;
                var value = input.extras[extra].value;
                switch (key) {
                	case "spatial":
						if (!output.extras) output.extras = [];
						output.extras.push({"key": key, "value": value});
	                	break;
	                default:
                }
            }
        }

        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].name;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];
                output.resources.push(resource);            
            }
        }

        return JSON.stringify(output);
    }
};
