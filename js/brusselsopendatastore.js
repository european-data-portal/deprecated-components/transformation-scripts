var jsontTemplate = { 

    "self": "{@schema($)}", 

    "schema": function(obj) {
        
        var output = {};
        
        output.name = input.name;
        output.title = input.title.fr;
        output.notes = input.notes.fr;

        if (input.author) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
            output.publisher.name = input.author;
            if (input.author_email) {
                output.publisher.email = "mailto:" + input.author_email;
            }            
        }
        if (!output.publisher && input.organization) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Organization";
            output.publisher.name = input.organization.title.fr;
        }

        if (input.maintainer) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input.maintainer;
            if (input.maintainer_email) {
                output.contact_point[0].email = "mailto:" + input.maintainer_email;
            }            
        }

        if (input.license_id) output.license_id = input.license_id;
        if (input.license_title) output.license_title = input.license_title;
        if (input.url) output.url = input.url;

        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                var t = {};
                t.name = input.tags[tag].name;
                output.tags.push(t);
            }
        }

        if (input.data_theme) {
            var groups = {};
            groups.AGRI = "agriculture-fisheries-forestry-and-food";
            groups.EDUC = "education-culture-and-sport";
            groups.ENVI = "environment";
            groups.ENER = "energy";
            groups.TRAN = "transport";
            groups.TECH = "science-and-technology";
            groups.ECON = "economy-and-finance";
            groups.SOCI = "population-and-society";
            groups.HEAL = "health";
            groups.GOVE = "government-and-public-sector";
            groups.REGI = "regions-and-cities";
            groups.JUST = "justice-legal-system-and-public-safety";
            groups.INTR = "international-issues";

            output.groups = [];
            for (var theme in input.data_theme) {
                var themename = input.data_theme[theme].slice(-4);
                if (themename in groups) {
                    output.groups.push({"name": groups[themename]});
                }
            }
        }

        if (input.spatial) {
            if (!output.extras) output.extras = [];
            output.extras.push({"key": "spatial", "value": input.spatial});
        }

        output.translation_meta = {
            "default": "fr"
        };
        output.translation = {
            "en": {
                "title": input.title.en,
                "notes": input.notes.en
            },
            "nl": {
                "title": input.title.nl,
                "notes": input.notes.nl
            }
        };

        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].name;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];
                if (input.resources[res].mimetype) resource.mediatype = input.resources[res].mimetype;
                output.resources.push(resource);            
            }
        }

        return JSON.stringify(output);
    }
};
